<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
	<UiMod name="Hopper" version="1.2.3" date="2010/11">
		<Author name="Wikki" email="wikkifizzle@gmail.com" />
		<Description text="Quickly change groups in a scenario or warband" />
		<VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="2.0" />
		<Dependencies>
			<Dependency name="EASystem_Utils" />
			<Dependency name="EASystem_WindowUtils" />
            <Dependency name="EATemplate_DefaultWindowSkin" />
        </Dependencies>
        
        <SavedVariables>
			<SavedVariable name="Hopper.Profiles" global="true"/>
		</SavedVariables>
		
		<Files>
			<File name="Libraries/LibStub.lua" />
			<File name="Libraries/LibGUI.lua" />
			<File name="Libraries/LibSharedAssets.lua" />
			<File name="Libraries/AceLocale-3.0.lua" />
			
			<File name="Localization/deDE.lua" />
			<File name="Localization/enUS.lua" />
			<File name="Localization/esES.lua" />
			<File name="Localization/frFR.lua" />
			<File name="Localization/itIT.lua" />
			<File name="Localization/jaJP.lua" />
			<File name="Localization/koKR.lua" />
			<File name="Localization/ruRU.lua" />
			<File name="Localization/zhCN.lua" />
			<File name="Localization/zhTW.lua" />
			
			<File name="Assets/HopperAssets.xml" />
	  		<File name="Source/HopperConstants.lua" />
	  		
	  		<File name="Configuration/HopperConfig.xml" />
			<File name="Configuration/HopperConfig.lua" />
			<File name="Configuration/HopperConfig_General.lua" />
			<File name="Configuration/HopperConfig_Profiles.lua" />
			<File name="Configuration/HopperConfig_About.lua" />
			
			<File name="Source/Hopper.xml" />
			<File name="Source/Hop.lua" />
			<File name="Source/Hopper.lua" />
			
			<File name="Source/Profiles.lua" />
		</Files>
		<OnInitialize>
			<CallFunction name="Hopper.OnInitialize" />
			<CreateWindow name="HopperConfig" show="false" />
		</OnInitialize>
		<OnShutdown>
			<CallFunction name="Hopper.OnShutdown" />
		</OnShutdown>
		<OnUpdate>
			<CallFunction name="Hopper.OnUpdate" />
			<CallFunction name="HopperConfig_General_OnUpdate" />
		</OnUpdate>
		
		<WARInfo>
		    <Categories>
		        <Category name="RVR" />
		        <Category name="GROUPING" />
		        <Category name="COMBAT" />
		    </Categories>
		    <Careers>
		        <Career name="DISCIPLE" />
		        <Career name="RUNE_PRIEST" />
		        <Career name="SHAMAN" />
		        <Career name="WARRIOR_PRIEST" />
		        <Career name="ZEALOT" />
		        <Career name="ARCHMAGE" />
		    </Careers>
		</WARInfo>

	</UiMod>
</ModuleFile>