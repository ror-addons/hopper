--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not Hopper then Hopper = {} end

--[===[@debug@
debug = true
--@end-debug@]===]

local alpha = false
--[===[@alpha@
alpha = true
--@end-alpha@]===]

local T 				= LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "Hopper", debug )
local LSA				= LibStub( "LibSharedAssets" )

local db

local VERSION 			= { MAJOR = 1, MINOR = 2, REV = 2 }
local DisplayVersion 	= string.format( "%d.%d.%d", VERSION.MAJOR, VERSION.MINOR, VERSION.REV )

if( debug ) then
	DisplayVersion 			= DisplayVersion .. " Dev Build"
else
	DisplayVersion 			= DisplayVersion .. " r" .. ( tonumber( "49" ) or 0 )
	
	if( alpha ) then
		DisplayVersion = DisplayVersion .. "-alpha"
	end
end

local firstLoad 				= true

local anchorId					= "HopperAnchor"
local windowBase				= "HopperWindow"

local hoppers					= {}
local C_MAX_BUTTONS				= 7

local bIsStarted				= false
local bIsInScenarioGroup		= false
local bSecenarioGroupIndex		= -1
local bWarbandGroupIndex		= -1

Hopper.C_MAX_PARYY_MEMBERS		= 6

local nextOnUpdateCheck			= 0
local currentOnUpdateTime		= 0
local bOnBattlegroupUpdated		= true

local playerName				= L""

local upgradeProfile

Hopper.DefaultSettings 	=
{
	["debug"]								= false,
	["version"]								= VERSION,
	
	["general-enable-addon"]				= true,
	["general-update-delay"]				= .5,
	["general-frame-alpha"]					= .5,
	
	["general-scenario-enable"]				= true,
	["general-warband-enable"]				= true,
	
	["hopper-fill-direction"]       		= "right",
    ["hopper-frame-vertical-offset"]		= 0,
	["hopper-frame-horizontal-offset"]		= 0,
	
	["activegroup-status-color"]			= {85, 188, 70},
	
	["slot-open-status-color"]				= {85, 188, 70},
	["slot-closed-status-color"]			= {255, 0, 0},
	["slot-status-alpha"]					= 1.0,
	
	["anchor-point"]						= "center",
    ["anchor-relpoint"]						= "center",
    ["anchor-relwin"]						= "Root",
    ["anchor-x"]                    		= 0,
    ["anchor-y"]                    		= 0,
    ["anchor-scale"]                		= 1.0,
    ["anchor-dx"]                   		= 40,
    ["anchor-dy"]                   		= 40,
}

function Hopper.GetVersion()
	return DisplayVersion
end

function Hopper.OnInitialize()
	RegisterEventHandler( SystemData.Events.RELOAD_INTERFACE, "Hopper.OnLoad" )
	RegisterEventHandler( SystemData.Events.LOADING_END, "Hopper.OnLoad" )
end

function Hopper.OnLoad()
	if( firstLoad ) then
		if( not DoesWindowExist( anchorId ) ) then CreateWindowFromTemplate( anchorId, anchorId, "Root" ) end
		
		-- Initialize our profiles
		Hopper.InitProfiles()
		
		-- Perform any profile upgrades necessary
		upgradeProfile()
		
		-- Register our slash commands with LibSlash
		Hopper.Debug( "Registering slash commands" )
		
		-- Register to know when the layout editor is complete
    	table.insert( LayoutEditor.EventHandlers, Hopper.OnLayoutEditorFinished )
    	
    	-- Load our configuration window
		HopperConfig.OnLoad()
    	
    	-- Create our hoppers
		for index = 1, C_MAX_BUTTONS
		do
			hoppers[index] = HopperWindow:Create( windowBase .. tostring( index ) )
			
			if( hoppers[index] == nil ) then
				d( "Error creating HopperWindow frame." )
				return
			end
			
			if( index < C_MAX_BUTTONS ) then
				hoppers[index]:SetButtonId( index )
			end
			
			hoppers[index]:Initialize()
			hoppers[index]:Show( false )
		end
		
		-- Print our initialization usage
		local init = WStringToString( T["Hopper %s initialized."] )
		Hopper.Print( init:format( DisplayVersion ) ) 
		
		-- Attempt to register one of our handlers
		if( LibSlash ~= nil and type( LibSlash.RegisterSlashCmd ) == "function" ) then
			LibSlash.RegisterSlashCmd( "hopper", Hopper.Slash )
			LibSlash.RegisterSlashCmd( "hopperconfig", Hopper.Slash )
			LibSlash.RegisterSlashCmd( "hop", Hopper.Slash )
			Hopper.Print( T["(Hopper) use /hopper to bring up configuration window."] )
		end

		-- Start hopper
		Hopper.Start()
		
		d( "Hopper loaded." )
	
		firstLoad = false
	end
	
	playerName = GameData.Player.name:match(L"([^^]+)^?([^^]*)")
end

function Hopper.OnShutdown()
	Hopper.Stop()
end

function Hopper.Restart()
	Hopper.Stop()
	Hopper.Start()
end

function Hopper.Start()
	if( not Hopper.Get( "general-enable-addon" ) or bIsStarted ) then return end
	
	-- Refresh all of our UI elements
	Hopper.Refresh()

	RegisterEventHandler( SystemData.Events.BATTLEGROUP_UPDATED, 					"Hopper.OnBattlegroupUpdated")
	RegisterEventHandler( SystemData.Events.SCENARIO_BEGIN, 						"Hopper.OnScenarioBegin" )
	RegisterEventHandler( SystemData.Events.SCENARIO_END, 							"Hopper.OnScenarioEnd" )
	RegisterEventHandler( SystemData.Events.SCENARIO_GROUP_JOIN, 					"Hopper.OnScenarioGroupJoin" )
	RegisterEventHandler( SystemData.Events.SCENARIO_GROUP_LEAVE, 					"Hopper.OnScenarioGroupLeave" )
	RegisterEventHandler( SystemData.Events.SCENARIO_PLAYERS_LIST_GROUPS_UPDATED, 	"Hopper.OnPlayerListGroupsUpdated" )
	RegisterEventHandler( SystemData.Events.CITY_SCENARIO_BEGIN, 					"Hopper.OnScenarioBegin")
	RegisterEventHandler( SystemData.Events.CITY_SCENARIO_END, 						"Hopper.OnScenarioEnd")
	
	bIsStarted = true
end

function Hopper.Stop()
	if( not bIsStarted ) then return end
	
	UnregisterEventHandler( SystemData.Events.BATTLEGROUP_UPDATED, 					"Hopper.OnBattlegroupUpdated" )
	UnregisterEventHandler( SystemData.Events.SCENARIO_BEGIN, 						"Hopper.OnScenarioBegin" )
    UnregisterEventHandler( SystemData.Events.SCENARIO_END, 						"Hopper.OnScenarioEnd" )
    UnregisterEventHandler( SystemData.Events.SCENARIO_GROUP_JOIN, 					"Hopper.OnScenarioGroupJoin" )
    UnregisterEventHandler( SystemData.Events.SCENARIO_GROUP_LEAVE, 				"Hopper.OnScenarioGroupLeave" )
    UnregisterEventHandler( SystemData.Events.SCENARIO_PLAYERS_LIST_GROUPS_UPDATED, "Hopper.OnPlayerListGroupsUpdated")
    UnregisterEventHandler( SystemData.Events.CITY_SCENARIO_BEGIN, 					"Hopper.OnScenarioBegin" )
	UnregisterEventHandler( SystemData.Events.CITY_SCENARIO_END, 					"Hopper.OnScenarioEnd" )
    
    for k,v in ipairs( hoppers )
	do
		v:Show( false )
	end
	
	bIsStarted = false
end

function Hopper.OnProfileChanged()
	local _, currentProfileName = Hopper.GetCurrentProfile()
	
	-- Upgrade the profile for any changes
	upgradeProfile()
	
	Hopper.Restart()
	
	Hopper.Print( L"(Hopper) " .. T["Active Profile:"] .. L" '" .. currentProfileName .. L"'" )
end

function Hopper.Refresh()
	Hopper.UpdateAnchorWindow()
    Hopper.UpdateAnchors()
    Hopper.UpdateButtonDisplay()
    Hopper.UpdateScenarioOpenSlots()
    Hopper.UpdateWarbandOpenSlots()
    
    for k,v in ipairs( hoppers )
	do
		v:Initialize()
	end
end

function Hopper.UpdateAnchorWindow()
	-- Remove our window from the layout editor
	LayoutEditor.UnregisterWindow( anchorId )
	
	-- Clear any anchors the window has
	WindowClearAnchors( anchorId )

	-- Set the anchor for its position on the screen
	WindowAddAnchor( anchorId, 
		Hopper.Get( "anchor-point" ), 
		Hopper.Get( "anchor-relwin" ), 
		Hopper.Get( "anchor-relpoint" ), 
		Hopper.Get( "anchor-x" ), 
		Hopper.Get( "anchor-y" ) )
	
	-- Set the window's scale
	WindowSetScale( anchorId, Hopper.Get( "anchor-scale" ) )
	WindowSetDimensions( anchorId, Hopper.Get("anchor-dx") / InterfaceCore.GetScale(), Hopper.Get( "anchor-dy" ) / InterfaceCore.GetScale() )
	
	-- Register with the layout editor
    LayoutEditor.RegisterWindow( anchorId, T["Hopper Anchor"], T["Hopper Anchor"], true, true, true, nil )
end

function Hopper.OnLayoutEditorFinished( exitCode )
	if( exitCode == LayoutEditor.EDITING_END ) then
        local point, relpoint, relwin, x, y = WindowGetAnchor( anchorId, 1 )
        
		Hopper.Set( "anchor-point", point )
        Hopper.Set( "anchor-relpoint", relpoint )
        Hopper.Set( "anchor-relwin", relwin )
        Hopper.Set( "anchor-x", x )
        Hopper.Set( "anchor-y", y )
        Hopper.Set( "anchor-scale", WindowGetScale( anchorId ) )
        
        Hopper.UpdateButtonDisplay()
    end
end

function Hopper.OnScenarioBegin()
	Hopper.UpdateButtonDisplay()
end

function Hopper.OnScenarioEnd()
	bIsInScenarioGroup 		= false
	Hopper.OnScenarioGroupLeave()
	Hopper.UpdateButtonDisplay()
end

function Hopper.OnScenarioGroupJoin( groupIndex )
	bIsInScenarioGroup 		= true
	bSecenarioGroupIndex	= groupIndex
	hoppers[groupIndex]:IsActive( true )
end

function Hopper.OnScenarioGroupLeave()
	if( hoppers[bSecenarioGroupIndex] ~= nil ) then
		hoppers[bSecenarioGroupIndex]:IsActive( false )
	end
	bSecenarioGroupIndex	= -1
end

function Hopper.OnPlayerListGroupsUpdated()
	Hopper.UpdateScenarioOpenSlots()
end

function Hopper.OnBattlegroupUpdated()
	bOnBattlegroupUpdated = true
end

function Hopper.OnUpdate( timeElapsed )
	if( bIsStarted ) then
		currentOnUpdateTime = currentOnUpdateTime + timeElapsed
		
		if( currentOnUpdateTime > nextOnUpdateCheck ) then
		
			if( bOnBattlegroupUpdated ) then
				Hopper.UpdateWarbandOpenSlots()
				bOnBattlegroupUpdated = false
			end
			
			nextOnUpdateCheck = currentOnUpdateTime + Hopper.Get( "general-update-delay" )
		end
	end
end

function Hopper.UpdateWarbandOpenSlots()
	if( GameData.Player.isInScenario or GameData.Player.isInSiege ) then return end
	
	local groupData = {}
	
	groupData = PartyUtils.GetWarbandData()
	
	local groupCount = {}
	
	-- Reset our active group flag
	if( bWarbandGroupIndex > 0 ) then hoppers[bWarbandGroupIndex]:IsActive( false ) end
	
	bWarbandGroupIndex = -1
	
	-- Calculate the group counts
	for groupIndex, group in ipairs( groupData )
	do
		if( groupCount[groupIndex] == nil ) then groupCount[groupIndex] = {} end
			
		-- Increment our member count
		groupCount[groupIndex] = #(group.players)
		
		for memberIndex, member in ipairs( group.players )
		do
			if( WStringsCompareIgnoreGrammer( member.name, GameData.Player.name ) == 0 ) then
				bWarbandGroupIndex = groupIndex
			end
		end
	end
	
	-- Update the slots accordingly
	Hopper.UpdateSlotCount( groupCount )
	
	-- Set our active group flag
	if( bWarbandGroupIndex > 0 ) then hoppers[bWarbandGroupIndex]:IsActive( true ) end
	 
	Hopper.UpdateButtonDisplay()
end

function Hopper.UpdateScenarioOpenSlots()
	-- Get our group data
	local groupData 	= GameData.GetScenarioPlayerGroups()
	local groupCount	= {}
	
	groupData = groupData or {}
	
	-- Iterate the data to find out which groups have an open slot
	for index, player in ipairs( groupData )
	do
		if( groupCount[player.sgroupindex] == nil ) then groupCount[player.sgroupindex] = 0 end
		groupCount[player.sgroupindex] = groupCount[player.sgroupindex] + 1
	end
	
	-- Update the slots accordingly
	Hopper.UpdateSlotCount( groupCount )
end

function Hopper.UpdateSlotCount( groupCount )
	for k,v in ipairs( hoppers )
	do
		if( groupCount[k] == nil ) then
			v:SlotCount( 0 )
		else
			v:SlotCount( groupCount[k] )
		end
	end
end

function Hopper.OnLButtonUp( flag )
	local groupIndex = tonumber( SystemData.ActiveWindow.name:match( "^" .. windowBase .. "(%d+)" ) )
	Hopper.SetGroup( groupIndex )
end

function Hopper.SetGroup( groupIndex )
	if( groupIndex ~= nil ) then
		-- If there is no room in the clicked group, dont bother processing
		if( hoppers[groupIndex] and hoppers[groupIndex]:GetSlotCount() < Hopper.C_MAX_PARYY_MEMBERS  ) then
			if( GameData.Player.isInScenario or GameData.Player.isInSiege ) then
				if( groupIndex ~= bSecenarioGroupIndex ) then
					LeaveScenarioGroup()
				end
				
				if( groupIndex < 7 ) then
					JoinScenarioGroup( groupIndex )
				end
			elseif( IsWarBandActive() ) then
				if( groupIndex ~= bWarbandGroupIndex ) then
					PartyUtils.MoveWarbandMember( playerName, groupIndex )
				end
			end
		end
	end
end

function Hopper.UpdateButtonDisplay()
	local displayCount 		= 0
	if( ( GameData.Player.isInScenario or GameData.Player.isInSiege ) and Hopper.Get( "general-scenario-enable" ) ) then
		displayCount = 7
	elseif( IsWarBandActive() and Hopper.Get( "general-warband-enable" ) ) then
		displayCount = 4
	end

	if( Hopper.Get( "debug" ) ) then
		displayCount = 7
	end
	
	-- We arent in any hoppable situation
	for k,v in ipairs( hoppers )
	do
		WindowSetScale( v:GetName(), Hopper.Get( "anchor-scale" ) )
		WindowSetDimensions( v:GetName(), Hopper.Get( "anchor-dx" ) / InterfaceCore.GetScale(), Hopper.Get( "anchor-dy" ) / InterfaceCore.GetScale() )
		--WindowSetDimensions( v:GetName(), Hopper.Get( "anchor-dx" ), Hopper.Get( "anchor-dy" ) )
		v:Show( k <= displayCount )
		--v:IsActive( false )
	end
	
	WindowSetShowing( anchorId, displayCount > 0 )
end

function Hopper.UpdateAnchors()
	local fillDirection				= Hopper.Get( "hopper-fill-direction" )
	local xOffset					= Hopper.Get( "hopper-frame-horizontal-offset" )
	local yOffset					= Hopper.Get( "hopper-frame-vertical-offset" )
	local currentAnchor 			= {}
	currentAnchor.Point             = "topleft"
	currentAnchor.RelativePoint     = "topleft"
	currentAnchor.RelativeTo        = anchorId
	currentAnchor.XOffset           = 0
	currentAnchor.YOffset           = 0 
	 
	for index, frame in ipairs( hoppers )
	do
		-- Clear the anchors for this frame
		frame:ClearAnchors()
		-- Set the new anchor for the frame
		frame:SetAnchor( currentAnchor )
		-- Get the anchor for the next frame
		currentAnchor = Hopper.GetNextAnchor( frame:GetName(), fillDirection, xOffset, yOffset )
	end
end

function Hopper.GetNextAnchor( windowId, location, xOffset, yOffset )
	local anchor = {}
	
	anchor.RelativeTo = windowId
	
	if( location == "up" ) then
		anchor.Point 			= "topleft"
		anchor.RelativePoint 	= "bottomleft"
		anchor.XOffset			= xOffset 
		anchor.YOffset			= yOffset * -1
	elseif( location == "left" ) then
		anchor.Point 			= "bottomleft"
		anchor.RelativePoint 	= "bottomright"
		anchor.XOffset			= xOffset * -1
		anchor.YOffset			= yOffset
	elseif( location == "right" ) then
		anchor.Point 			= "bottomright"
		anchor.RelativePoint 	= "bottomleft"
		anchor.XOffset			= xOffset 
		anchor.YOffset			= yOffset
	else
		anchor.Point 			= "bottomleft"
		anchor.RelativePoint 	= "topleft"
		anchor.XOffset			= xOffset
		anchor.YOffset			= yOffset
	end

	return anchor
end

local recursions = {}
local function better_toString(data, depth)
    if type(data) == "string" then
        return ("%q"):format(data)
    elseif type(data) == "wstring" then
        return ("w%q"):format(WStringToString(data))
    elseif type(data) ~= "table" then
        return ("%s"):format(tostring(data))
    else
        if recursions[data] then
            return "{<recursive table>}"
        end
        recursions[data] = true
        if next(data) == nil then
            return "{}"
        elseif next(data, next(data)) == nil then
            return "{ [" .. better_toString(next(data), depth) .. "] = " .. better_toString(select(2, next(data)), depth) .. " }"
        else
            local t = {}
            t[#t+1] = "{\n"
            local keys = {}
            for k in pairs(data) do
                keys[#keys+1] = k
            end
            table.sort(keys, mysort)
            for _, k in ipairs(keys) do
                local v = data[k]
                for i = 1, depth do
                    t[#t+1] = "    "
                end
                t[#t+1] = "["
                t[#t+1] = better_toString(k, depth+1)
                t[#t+1] = "] = "
                t[#t+1] = better_toString(v, depth+1)
                t[#t+1] = ",\n"
            end
            
            for i = 1, depth do
                t[#t+1] = "    "
            end
            t[#t+1] = "}"
            return table.concat(t)
        end
    end
end

local function pformat(...)
    local n = select('#', ...)
    local t = {'value: '}
    for i = 1, n do
        if i > 1 then
            t[#t+1] = ", "
        end
        t[#t+1] = better_toString((select(i, ...)), 0)
    end
    for k in pairs(recursions) do
        recursions[k] = nil
    end
    return table.concat(t), n
end

function Hopper.Slash( input )
	Hopper.ToggleConfig()
end

function Hopper.ToggleConfig()
	if( DoesWindowExist( HopperConfig.GetWindowName() ) ) then
		WindowSetShowing( HopperConfig.GetWindowName(), not WindowGetShowing( HopperConfig.GetWindowName() ) )
	else
		Hopper.Print("[Hopper] is installed. Use '/Hopper <option>' to view a setting, and '/Hopper <option> <value>' to change one. To get a list of known options, use '/Hopper options'.")		
	end
end

function Hopper.Print( wstr )
	local diplay = towstring( wstr )
	EA_ChatWindow.Print( diplay, ChatSettings.Channels[SystemData.ChatLogFilters.SAY].id );
end

function Hopper.Debug(str)
	if( Hopper.Get( "debug" ) ) then
	 	DEBUG(towstring(str))
	end
end

function upgradeProfile()
	local oldVersion = Hopper.Get( "version" )
	Hopper.Set( "version", VERSION )
	
	-- add any processing here on version change
end