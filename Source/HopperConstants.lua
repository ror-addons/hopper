--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not HopperConstants then HopperConstants = {} end

local T 	= LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "Hopper" )

-- Anchor Configuration Information    ( "top", "bottom", "left", "right" )
HopperConstants.Anchors		= {}
HopperConstants.Anchors[1]	= { config="top", 		locale=T["Top"] }
HopperConstants.Anchors[2]	= { config="bottom", 	locale=T["Bottom"] }
HopperConstants.Anchors[3]	= { config="left", 		locale=T["Left"] }
HopperConstants.Anchors[4]	= { config="right", 	locale=T["Right"] }

HopperConstants.AnchorsLookup	= {}
for k,v in ipairs( HopperConstants.Anchors )
do
	HopperConstants.AnchorsLookup[v.config] = k
end

-- Growth Direction Configuration Information ( "up", "down", "left", "right" )
HopperConstants.GrowthDirections		= {}
HopperConstants.GrowthDirections[1]	= { config="up", 		locale=T["Up"] }
HopperConstants.GrowthDirections[2]	= { config="down", 		locale=T["Down"] }
HopperConstants.GrowthDirections[3]	= { config="left", 		locale=T["Left"] }
HopperConstants.GrowthDirections[4]	= { config="right", 	locale=T["Right"] }

HopperConstants.GrowthDirectionsLookup	= {}
for k,v in ipairs( HopperConstants.GrowthDirections )
do
	HopperConstants.GrowthDirectionsLookup[v.config] = k
end