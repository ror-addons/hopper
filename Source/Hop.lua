--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not HopperWindow then HopperWindow = Frame:Subclass( "HopperWindow" ) end

function HopperWindow:Create( windowName )
	local unitFrame = self:CreateFromTemplate( windowName )
        
    if( unitFrame == nil ) then return nil end
    
    unitFrame.m_windowId		= windowName
	unitFrame.m_Id				= 0
	unitFrame.m_SlotCount		= 0
	
	unitFrame:IsActive( false )
	unitFrame:SlotCount( 0 )
	
	return unitFrame
end

function HopperWindow:Initialize()
	local windowId = self:GetName()
	
	WindowSetAlpha( windowId, Hopper.Get( "general-frame-alpha" ) )
	WindowSetAlpha( windowId .. "Slot", Hopper.Get( "slot-status-alpha" ) )
end

function HopperWindow:SetButtonId( id )
	self.m_Id = id
	LabelSetText( self:GetName() .. "Display", towstring( id ) )
end

function HopperWindow:IsActive( active )
	local color = {255,255,255} 
	
	if( active ) then
		color = Hopper.Get( "activegroup-status-color" )
	end
	
	LabelSetTextColor( self:GetName() .. "Display", unpack( color ) )
end

function HopperWindow:SlotCount( slotCount )
	local windowId = self:GetName()
	local color = {255,255,255}
	
	self.m_SlotCount = slotCount
	
	if( slotCount == Hopper.C_MAX_PARYY_MEMBERS ) then
		color = Hopper.Get( "slot-closed-status-color" )
	elseif( slotCount > 0 ) then
		color = Hopper.Get( "slot-open-status-color" )
	end
	
	WindowSetTintColor( windowId .. "Slot", unpack( color ) )
	WindowSetShowing( windowId .. "Slot", slotCount > 0 )
end

function HopperWindow:GetId()
	return self.m_Id
end

function HopperWindow:GetSlotCount()
	return self.m_SlotCount
end	