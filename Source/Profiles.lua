--
-- This file adapted from SquaredProfiles.lua, from Aiiane's Squared, to work with this addon
--
local Hopper = Hopper

-- If it doesn't already exist, make it
if not Hopper.Profiles then Hopper.Profiles = {active=0,names={},data={},perchar={}} end

-- Make utility functions local for performance
local pairs 			= pairs
local unpack 			= unpack
local tonumber			= tonumber
local tostring 			= tostring
local towstring 		= towstring
local max 				= math.max
local min 				= math.min
local wstring_sub 		= wstring.sub
local wstring_format 	= wstring.format
local tinsert 			= table.insert
local tremove 			= table.remove

-- generic deepcopy
local function deepcopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return new_table
    end
    return _copy(object)
end

-- This function takes a defaults table and a settings, and verifies that all 
-- keys that exist in the defaults exist in the settings.
local function updateSettings(defaults, settings)
	if type(defaults) ~= "table" or type(settings) ~= "table" then return end
	for key, value in pairs( defaults ) do
		if type(value) ~= "table" and value ~= nil then
			-- Check if the key exists in settings
			if settings[key] == nil then
				settings[key] = value
			end
		else
			-- Create the table if it doesnt exist, or if its type is not of table
			if settings[key] == nil or type(settings[key]) ~= "table" then
				settings[key] = {}
			end
			
			updateSettings( value, settings[key] )
		end
	end	
end

local function GetCurrentSlot()
	for k,v in pairs(GameData.Account.CharacterSlot) do
		if v.Name == GameData.Player.name then
			return k
		end
	end
	return nil
end

function Hopper.Set(key, value)
	if key then
		Hopper.Profiles.data[Hopper.Profiles.active][key] = value
	end
end

function Hopper.Get( key )
	if key then
		return Hopper.Profiles.data[Hopper.Profiles.active][key]
	end
	return nil
end

function Hopper.ResetActiveProfile()
	if Hopper.Profiles and Hopper.Profiles.data and Hopper.Profiles.data[Hopper.Profiles.active] then
		Hopper.Profiles.data[Hopper.Profiles.active] = deepcopy(Hopper.DefaultSettings)
		Hopper.OnProfileChanged()
	end
end

-- Initialize the profile system (called from Hopper.OnLoad)
function Hopper.InitProfiles()
    if Hopper.Profiles.active == 0 then
		-- Empty profiles table - initialize the 'Default' profile
		Hopper.Profiles.active = Hopper.AddProfile(L"Default")
		local slot = GetCurrentSlot()
		if not slot then d("Couldn't figure out what character slot this is!") return end
		Hopper.Profiles.perchar[slot] = L"Default"
	else
		-- Non-empty profiles table - check for smart activation
		local slot = GetCurrentSlot()
		local profile = Hopper.Profiles.perchar[slot]
		if profile then
			Hopper.ActivateProfile(profile)
		else 
			Hopper.Profiles.perchar[slot] = L"Default"
			Hopper.ActivateProfile(L"Default")
		end
	end
end

function Hopper.AddProfile(name, source)
	local sourceId = nil
	
	-- First check to see if we're adding a profile which already exists,
	-- if we are then just return that profile's id
	for k,v in ipairs(Hopper.Profiles.names) do
		if v == name then return k end
		-- Might as well find the source Id while we're at it
		if v == source then sourceId = k end
	end
	
	-- If it doesn't exist, add it
	tinsert(Hopper.Profiles.names, name)
	local data = Hopper.GetProfileData(sourceId)
	if data then
		tinsert(Hopper.Profiles.data, data)
	else
		tinsert(Hopper.Profiles.data, deepcopy(Hopper.DefaultSettings))
	end
	
	return #Hopper.Profiles.names
end

function Hopper.GetProfileData(source)
	-- If specified by name, look up the id
	if type(source) == "wstring" then
		for k,v in ipairs(Hopper.Profiles.names) do
			if v == source then
				source = k
				break
			end
		end
	end
	
	if type(source) == "number" then
		return deepcopy(Hopper.Profiles.data[source])
	else
		-- invalid profile
		return nil
	end
end

function Hopper.GetProfileDataForUse(source)
	-- If specified by name, look up the id
	if type(source) == "wstring" then
		for k,v in ipairs(Hopper.Profiles.names) do
			if v == source then
				source = k
				break
			end
		end
	end
	
	if type(source) == "number" then
		return Hopper.Profiles.data[source]
	else
		-- invalid profile
		return nil
	end
end

function Hopper.ActivateProfile(source, force)
	-- Find the profile if specified by name
	if type(source) == "wstring" then
		for k,v in ipairs(Hopper.Profiles.names) do
			if v == source then source = k end
		end
	end
	
	if type(source) ~= "number" then return end
	local sourceName = Hopper.Profiles.names[source]
	if not force and Hopper.Profiles.active == source then return end
	
	local data = Hopper.GetProfileData(source)
	if not data then return end
	Hopper.Profiles.active = source
	
	-- Make sure the profile is up to date
	updateSettings( Hopper.DefaultSettings, Hopper.Profiles.data[Hopper.Profiles.active] )
	
	Hopper.OnProfileChanged()
	
	local slot = GetCurrentSlot()
	if slot then
		Hopper.Profiles.perchar[slot] = sourceName
	end

	return source
end

function Hopper.CopyProfileToActive( source )
	-- Find the profile if specified by name
	if type(source) == "wstring" then
		for k,v in ipairs(Hopper.Profiles.names) do
			if v == source then source = k end
		end
	end
	if type(source) ~= "number" then return end
	
	if Hopper.Profiles and Hopper.Profiles.data and Hopper.Profiles.data[source] and Hopper.Profiles.data[Hopper.Profiles.active] then
		Hopper.Profiles.data[Hopper.Profiles.active] = deepcopy(Hopper.Profiles.data[source])
		
		updateSettings( Hopper.DefaultSettings, Hopper.Profiles.data[Hopper.Profiles.active] )
		
		Hopper.OnProfileChanged()
	end
end

function Hopper.GetProfileList()
	return deepcopy(Hopper.Profiles.names)
end

function Hopper.GetCurrentProfile()
	return Hopper.Profiles.active, Hopper.Profiles.names[Hopper.Profiles.active]
end

function Hopper.DeleteProfile(source)
	-- Find the profile if specified by name
	if type(source) == "wstring" then
		for k,v in ipairs(Hopper.Profiles.names) do
			if v == source then source = k end
		end
	end
	
	if type(source) ~= "number" then return end
	
	-- Don't allow people to delete the default profile, or a nonexistant one
	if source < 2 or not Hopper.Profiles.names[source] then return end
	
	local sourceName = Hopper.Profiles.names[source]
	
	-- Remove the profile
	tremove(Hopper.Profiles.names, source)
	tremove(Hopper.Profiles.data, source)
	
	-- Swap any perchars which were the deleted profile back to default
	local slot = GetCurrentSlot()
	for k,v in pairs(Hopper.Profiles.perchar) do
		if v == sourceName then
			Hopper.Profiles.perchar[k] = L"Default"
			-- If our current character was set to the deleted profile, activate default
			if k == slot then
				Hopper.ActivateProfile(L"Default")
			end
		end
	end
end