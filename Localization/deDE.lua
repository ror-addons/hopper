--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/hopper/localization/
--

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("Hopper", "deDE")
if not T then return end

T["About Hopper"] = L"�ber Hopper"
T["Apply"] = L"Anwenden"
T["Bottom"] = L"Unten"
T["Close"] = L"Schlie�en"
T["Coming Soon!"] = L"In Arbeit!"
T["Copy Settings From:"] = L"Einstellungen kopieren von:"
-- T["Create"] = L""
-- T["Create New Profile"] = L""
T["Current Profile:"] = L"Aktuelles Profil:"
-- T["Delete"] = L""
T["Down"] = L"Runter"
T["Enable Hopper"] = L"Hopper aktivieren"
T["General"] = L"Allgemein"
T["Growth Direction"] = L"Ausrichtung"
T["Growth Direction:"] = L"Ausrichtung"
T["Hopper %s initialized."] = L"Hopper %s initialisiert."
T["Hopper - Configuration"] = L"Hopper - Konfiguration"
T["Hopper Anchor"] = L"Hopper Anker"
T["Left"] = L"Links"
T["NOTE: Profile changes take effect immediately!"] = L"ACHTUNG: Profil�nderungen sind sofort wirksam!"
T["Profiles"] = L"Profile"
T["Reset Profile"] = L"Profil zur�cksetzen"
T["Revert"] = L"R�ckg�ngig"
T["Right"] = L"Rechts"
T["Set Active Profile:"] = L"Aktives Profil ausw�hlen:"
T["Show In Scenarios"] = L"In Szenarien anzeigen"
T["Show In Warband"] = L"Im Kriegstrupp anzeigen"
T["Top"] = L"Oben"
T["Up"] = L"Hoch"
T["WARNING: Clicking this button will restore the current profile back to default values!"] = L"ACHTUNG: Das aktuelle Profil wird mit Klicken sofort auf die Standard-Einstellungen zur�ckgesetzt."

