--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/hopper/localization/
--

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("Hopper", "itIT")
if not T then return end

T["About Hopper"] = L"Riguardo Hopper"
T["Apply"] = L"Applica"
T["Bottom"] = L"Sotto"
T["Close"] = L"Chiudi"
T["Coming Soon!"] = L"Prossimamente!"
T["Copy Settings From:"] = L"Copia impostazioni da:"
-- T["Create"] = L""
-- T["Create New Profile"] = L""
T["Current Profile:"] = L"Profilo attuale:"
-- T["Delete"] = L""
T["Down"] = L"Gi�"
T["Enable Hopper"] = L"Abilita Hopper"
T["General"] = L"Generale"
T["Growth Direction"] = L"Direzione riempimento"
T["Growth Direction:"] = L"Direzione riempimento:"
T["Hopper %s initialized."] = L"Hopper %s inizializzato."
T["Hopper - Configuration"] = L"Hopper - Configurazione"
T["Hopper Anchor"] = L"Posizionamento Hopper"
T["Left"] = L"Sinistra"
T["NOTE: Profile changes take effect immediately!"] = L"NOTA: i cambiamenti al profilo avvengono immediatamente!"
T["Profiles"] = L"Profili"
T["Reset Profile"] = L"Azzera profilo"
T["Revert"] = L"Ripristina"
T["Right"] = L"Destra"
T["Set Active Profile:"] = L"Imposta profilo attivo:"
T["Show In Scenarios"] = L"Mostra negli Scenari"
T["Show In Warband"] = L"Mostra in plotone"
T["Top"] = L"Sopra"
T["Up"] = L"Su"
T["WARNING: Clicking this button will restore the current profile back to default values!"] = L"ATTENZIONE: premendo questo pulsante il profilo attuale torner� ai valori iniziali!"

