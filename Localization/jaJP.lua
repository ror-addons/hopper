--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/hopper/localization/
--

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("Hopper", "jaJP")
if not T then return end

-- T["About Hopper"] = L""
-- T["Apply"] = L""
-- T["Bottom"] = L""
-- T["Close"] = L""
-- T["Coming Soon!"] = L""
-- T["Copy Settings From:"] = L""
-- T["Create"] = L""
-- T["Create New Profile"] = L""
-- T["Current Profile:"] = L""
-- T["Delete"] = L""
-- T["Down"] = L""
-- T["Enable Hopper"] = L""
-- T["General"] = L""
-- T["Growth Direction"] = L""
-- T["Growth Direction:"] = L""
-- T["Hopper %s initialized."] = L""
-- T["Hopper - Configuration"] = L""
-- T["Hopper Anchor"] = L""
-- T["Left"] = L""
-- T["NOTE: Profile changes take effect immediately!"] = L""
-- T["Profiles"] = L""
-- T["Reset Profile"] = L""
-- T["Revert"] = L""
-- T["Right"] = L""
-- T["Set Active Profile:"] = L""
-- T["Show In Scenarios"] = L""
-- T["Show In Warband"] = L""
-- T["Top"] = L""
-- T["Up"] = L""
-- T["WARNING: Clicking this button will restore the current profile back to default values!"] = L""

