--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/hopper/localization/
--

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ):NewLocale( "Hopper", "enUS", true, debug )

T["About Hopper"] = L"About Hopper"
T["Apply"] = L"Apply"
T["Bottom"] = L"Bottom"
T["Close"] = L"Close"
T["Coming Soon!"] = L"Coming Soon!"
T["Copy Settings From:"] = L"Copy Settings From:"
T["Create"] = L"Create"
T["Create New Profile"] = L"Create New Profile"
T["Current Profile:"] = L"Current Profile:"
T["Delete"] = L"Delete"
T["Down"] = L"Down"
T["Enable Hopper"] = L"Enable Hopper"
T["General"] = L"General"
T["Growth Direction"] = L"Growth Direction"
T["Growth Direction:"] = L"Growth Direction:"
T["Hopper %s initialized."] = L"Hopper %s initialized."
T["Hopper - Configuration"] = L"Hopper - Configuration"
T["Hopper Anchor"] = L"Hopper Anchor"
T["Left"] = L"Left"
T["NOTE: Profile changes take effect immediately!"] = L"NOTE: Profile changes take effect immediately!"
T["Profiles"] = L"Profiles"
T["Reset Profile"] = L"Reset Profile"
T["Revert"] = L"Revert"
T["Right"] = L"Right"
T["Set Active Profile:"] = L"Set Active Profile:"
T["Show In Scenarios"] = L"Show In Scenarios"
T["Show In Warband"] = L"Show In Warband"
T["Top"] = L"Top"
T["Up"] = L"Up"
T["WARNING: Clicking this button will restore the current profile back to default values!"] = L"WARNING: Clicking this button will restore the current profile back to default values!"
