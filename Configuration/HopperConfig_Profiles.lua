--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not HopperConfig then return end

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T 		= LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "Hopper", debug )
local LibGUI 	= LibStub( "LibGUI" )

local initialized		= false

local InitializeWindow, ApplyConfigSettings, RevertConfigSettings

local window	= {
	Name		= T["Profiles"],
	display		= {},
	W			= {},
}

function InitializeWindow()
	-- If our window already exists, we are all set
	if initialized then return end
	
	local w
	local e	
	
	-- General Window
	w = LibGUI( "window" )
	w:ClearAnchors()
	w:Resize( 400, 400 )
	w:Show( true )
	window.W.General = w
	
	-- This is the order the windows are displayed to the user
	table.insert( window.display, window.W.General )
	
	--
	-- GENERAL WINDOW
	--
	-- General - Subsection Title
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General, "topleft", "topleft", 15, 10 )
    e:Font( "font_clear_medium_bold" )
    e:SetText( T["Profiles"] )
    window.W.General.Title_Label = e
    
    -- General - Set Active Profile Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.Title_Label, "bottomleft", "topleft", 5, 0 )
    --e:AnchorTo( window.W.General.CurrentProfileDesc_Label, "bottomleft", "topleft", 0, 25 )
    e:Font( "font_chat_text" )
    e:SetText( T["Set Active Profile:"] )
    window.W.General.SetActiveProfile_Label = e
    
    -- General - Set Active Profile Combo
    e = window.W.General( "combobox", nil, "HopperProfileComboBox_ActiveProfile" )
    e:AnchorTo( window.W.General.SetActiveProfile_Label, "bottomleft", "topleft", 10, 0 )
    window.W.General.SetActiveProfile_Combo = e
    
    -- General - Delete Profile Button
    e = window.W.General( "Button", nil, "EA_Button_DefaultResizeable" )
    e:Resize( 100 )
    e:Show( true )
    e:AnchorTo( window.W.General.SetActiveProfile_Combo, "right", "left", 5, 0 )
    e:SetText( T["Delete"] )
    e.OnLButtonUp = 
		function()
			HopperConfig_Profiles_DeleteProfile()
		end
    window.W.General.DeleteProfile_Button = e
    
    -- General - Copy Settings From Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.SetActiveProfile_Label, "bottomleft", "topleft", 0, 30 )
    e:Font( "font_chat_text" )
    e:SetText( T["Copy Settings From:"] )
    window.W.General.CopySettingsFrom_Label = e
    
    -- General - Copy Settings Combo
    e = window.W.General( "combobox", nil, "HopperProfileComboBox_CopyProfile" )
    e:AnchorTo( window.W.General.CopySettingsFrom_Label, "bottomleft", "topleft", 10, 0 )
    window.W.General.CopySettingsFrom_Combo = e
    
    -- General - Create New Profile Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.CopySettingsFrom_Label, "bottomleft", "topleft", 0, 30 )
    e:Font( "font_chat_text" )
    e:SetText( T["Create New Profile:"] )
    window.W.General.CreateNewProfile_Label = e
    
    -- General - Create New Profile Textbox
    e = window.W.General( "Textbox" )
    e:Resize( 400 )
    e:AnchorTo( window.W.General.CreateNewProfile_Label, "bottomleft", "topleft", 10, 0 )
    e:SetText( L"" );
    window.W.General.CreateNewProfile_Textbox = e
    
    -- General - Create New Profile Button
    e = window.W.General( "Button", nil, "EA_Button_DefaultResizeable" )
    e:Resize( 100 )
    e:Show( true )
    e:AnchorTo( window.W.General.CreateNewProfile_Textbox, "right", "left", 5, 0 )
    e:SetText( T["Create"] )
    e.OnLButtonUp = 
		function()
			HopperConfig_Profiles_CreateNewProfile()
		end
    window.W.General.CreateNewProfile_Button = e
    
    -- General - Note Label
    e = window.W.General( "Label" )
    e:Resize( 500, 40 )
    e:Align( "center" )
    e:AnchorTo( window.W.General.CreateNewProfile_Label, "bottomleft", "topleft", 0, 35 )
    e:Font( "font_chat_text" )
    e:Color( 255, 255, 255 )
    e:WordWrap( true )
    e:SetText( T["NOTE: Profile changes take effect immediately!"] )
    window.W.General.Note_Label = e

    -- Reset - Reset Configuration Button
    e = window.W.General( "Button", nil, "EA_Button_DefaultResizeable" )
    e:Resize( 350 )
    e:Show( true )
    e:AnchorTo( window.W.General, "top", "top", 0, 350 )
    e:SetText( T["Reset Profile"] )
    e.OnLButtonUp = 
		function()
			DialogManager.MakeTwoButtonDialog(  T["Are you sure?"], 
                                            GetString( StringTables.Default.LABEL_YES ), HopperConfig_Profiles_OnResetProfile, 
                                            GetString( StringTables.Default.LABEL_NO ),  nil,
											nil, nil, nil, nil, DialogManager.TYPE_MODAL )
		end
    window.W.General.ResetConfiguration_Button = e
    
    -- General - Warning Label
    e = window.W.General( "Label" )
    e:Resize( 500, 90 )
    e:Align( "center" )
    e:AnchorTo( window.W.General.ResetConfiguration_Button, "top", "bottom", 0, 0 )
    e:Font( "font_chat_text" )
    e:Color( 255, 0, 0 )
    e:WordWrap( true )
    e:SetText( T["WARNING: Clicking this button will restore the current profile back to default values!"] )
    window.W.General.Warning_Label = e
    
    initialized = true
end

function ApplyConfigSettings()
end

function RevertConfigSettings()

	local currentProfileIdx , currentProfileName = Hopper.GetCurrentProfile()
	local populated = false
	
	--
	-- General Settings
	--
	--window.W.General.CurrentProfile_Label:SetText( currentProfileName )
	
	--
	-- SET ACTIVE PROFILE SECTION
	--
	window.W.General.SetActiveProfile_Combo:Clear()
	for k,v in pairs(Hopper.GetProfileList()) do
        window.W.General.SetActiveProfile_Combo:Add(v)
		populated = true
    end
    
    if populated then
        window.W.General.SetActiveProfile_Combo:SelectIndex( currentProfileIdx )
    end

	--
	-- COPY SETTINGS
	--	
	populated = false
	window.W.General.CopySettingsFrom_Combo:Clear()
	for k,v in pairs(Hopper.GetProfileList()) do
		if v ~= currentProfileName then
        	window.W.General.CopySettingsFrom_Combo:Add(v)
			populated = true
		end
    end
end

function HopperConfig_Profiles_OnActiveProfileChanged()
	local profile = window.W.General.SetActiveProfile_Combo:Selected()
    if not profile then return end
    Hopper.ActivateProfile( profile )
    HopperConfig.OnRevert()
end

function HopperConfig_Profiles_OnCopyProfileChanged()
	local profileCopy = window.W.General.CopySettingsFrom_Combo:Selected()
    if not profileCopy then return end
	
	Hopper.CopyProfileToActive( profileCopy )
	
	HopperConfig.OnRevert()
end

function HopperConfig_Profiles_CreateNewProfile()
	local newProfile = window.W.General.CreateNewProfile_Textbox:GetText()
    if not newProfile then return end
	
	local profile = window.W.General.SetActiveProfile_Combo:Selected()
    if not profile then return end
	
	local id = Hopper.AddProfile(newProfile, profile)
    
	Hopper.ActivateProfile(id)
	
	window.W.General.CreateNewProfile_Textbox:SetText( L"" )
	
    HopperConfig.OnRevert()
end

function HopperConfig_Profiles_DeleteProfile()
	local profile = window.W.General.SetActiveProfile_Combo:Selected()
    if not profile then return end
    
    Hopper.DeleteProfile(profile)
    
    HopperConfig.OnRevert()
end

function HopperConfig_Profiles_OnResetProfile()
	Hopper.ResetActiveProfile()
	HopperConfig.OnRevert()
end

window.Initialize	= InitializeWindow
window.Apply		= ApplyConfigSettings
window.Revert		= RevertConfigSettings
window.Index		= HopperConfig.RegisterWindow( window )