--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not HopperConfig then HopperConfig = {} end

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "Hopper", debug )

local windowId 					= "HopperConfig"

local configWindowScroll		= windowId .. "Scroll"
local configWindowScrollChild	= configWindowScroll .. "Child"
local MAX_VISIBLE_ROWS 			= 15

-- Our list of registered windows
local configuredWindows			= {}

local displayOrder 				= {}		
HopperConfig.displayData 		= {}

local currentDisplayedConfig	= 1

function HopperConfig.GetWindowName() return windowId end
function HopperConfig.IsShowing() return WindowGetShowing( windowId ) end
function HopperConfig.GetActiveConfigWindowIndex() return currentDisplayedConfig end

function HopperConfig.OnLoad()
	-- Set our configuration title
	LabelSetText( windowId .. "TitleBarText", T["Hopper - Configuration"] )
	
	-- Set our highlighting for the listbox
	DataUtils.SetListRowAlternatingTints( windowId .. "List", MAX_VISIBLE_ROWS )
	
	-- Create our window display data
	HopperConfig.CreateDisplayData()
	
	-- Display the window list
	HopperConfig.UpdateConfigList()
	
	-- Initialize our configuration windows
	for index, window in ipairs( configuredWindows )
    do
    	-- Create and store our window
    	window:Initialize()
    	
    	-- Hide the config dialog
    	HopperConfig.HideConfig( index )
    end
    
    -- Display our initial window
    HopperConfig.UpdateDisplayedConfig( currentDisplayedConfig )
    
    -- Revert all of our windows to set their settings
	HopperConfig.RevertDialog()
	
	-- Set our close button title
	LabelSetText( windowId .. "Version", towstring( Hopper.GetVersion() ) )
	
	-- Set our close button title
	ButtonSetText( windowId .. "CloseButton", T["Close"] )	 
	
	-- Set our revert button title
	ButtonSetText( windowId .. "RevertButton", T["Revert"] )
	
	-- Set our apply button title
	ButtonSetText( windowId .. "ApplyButton", T["Apply"] )
end

function HopperConfig.CreateDisplayData()
	-- Clear any existing data
	HopperConfig.displayData = {}
	
	-- Create our list of configuration pages
	for slotNum, window in pairs( configuredWindows ) 
	do
		local listDisplayItem = {}
		
		listDisplayItem.slotNum 	= slotNum
		listDisplayItem.Name		= window.Name
    
    	HopperConfig.displayData[slotNum] = listDisplayItem
	end
end

function HopperConfig.UpdateConfigList()
	-- Clear our current display data
	displayOrder = {}
    
    -- Create the list we will use to display
    for index,_ in ipairs( HopperConfig.displayData )
    do
    	-- Add this to the end of our display
    	table.insert( displayOrder, index )
    end
    
    -- Display the data
    ListBoxSetDisplayOrder( windowId .. "List", displayOrder )
end

function HopperConfig.PopulateDisplay()
	local slotNum, config
	 
	for row, data in ipairs( HopperConfigList.PopulatorIndices ) 
	do
		HopperConfig.UpdateListRow( HopperConfig.displayData[data], windowId .. "ListRow".. row )
	end 
end

function HopperConfig.UpdateListRow( config, rowName )
	if( config.slotNum == currentDisplayedConfig ) then
		LabelSetTextColor( rowName .. "Name", DefaultColor.GREEN.r, DefaultColor.GREEN.g, DefaultColor.GREEN.b )
	else
		LabelSetTextColor( rowName .. "Name", 255 ,255 ,255 )
	end
end

function HopperConfig.OnLButtonUpConfigList()
	local rowNumber, slotNumber, config = HopperConfig.GetSlotRowNumForActiveListRow()
	
	if( slotNumber ~= currentDisplayedConfig ) then
		HopperConfig.UpdateDisplayedConfig( slotNumber )	
	end
end

function HopperConfig.UpdateDisplayedConfig( newConfig )
	local prevWindow = configWindowScrollChild
	
	-- Hide the old config
	HopperConfig.HideConfig( currentDisplayedConfig )
	
	-- Clear the old config parent
	HopperConfig.SetConfigParent( currentDisplayedConfig, "Root" )
	
	-- Set the new config parent
	HopperConfig.SetConfigParent( newConfig, configWindowScrollChild )
	
	-- Anchor the configs children
	HopperConfig.ReanchorConfigDisplay( newConfig, configWindowScrollChild )
	
	-- Display the new config
	HopperConfig.ShowConfig( newConfig )
	
	-- Update our current displayed config
	currentDisplayedConfig = newConfig
	
	-- Tell our scroll window to update its scroll window
	ScrollWindowSetOffset( configWindowScroll, 0 )
	ScrollWindowUpdateScrollRect( configWindowScroll )

	-- Repopulate our display
	HopperConfig.PopulateDisplay()
end

function HopperConfig.GetSlotRowNumForActiveListRow()
	local rowNumber, slowNumber, config
	
	-- Get the row within the window
	rowNumber = WindowGetId( SystemData.ActiveWindow.name ) 

	-- Get the data index from the list box
    local dataIndex = ListBoxGetDataIndex( windowId .. "List" , rowNumber )
    
    -- Get the slot from the data
    if( dataIndex ~= nil ) then
    	
    	slotNumber = HopperConfig.displayData[dataIndex].slotNum
    
	    -- Get the data
	    if( slotNumber ~= nil ) then
	    	config = configuredWindows[slotNumber]
	    end
	end
    
	return slotNumber, rowNumber, config, window
end

function HopperConfig.SaveDialog()
	-- Revert all of our windows to set their settings
	for index, window in ipairs( configuredWindows )
    do
    	window:Apply()
    end
end

function HopperConfig.RevertDialog()
	-- Revert all of our windows to set their settings
	for index, window in ipairs( configuredWindows )
    do
    	window:Revert()
    end
end

function HopperConfig.OnClose()
	WindowSetShowing( windowId, false )
end

function HopperConfig.OnApply()
	-- Save the dialog
	HopperConfig.SaveDialog()
	
	-- Refresh the unit frames
	Hopper.Restart()
end

function HopperConfig.OnRevert()
	HopperConfig.RevertDialog()
end

function HopperConfig.RegisterWindow( window )
	table.insert( configuredWindows, window )
	return #configuredWindows
end

function HopperConfig.OnResizeBegin()
    WindowUtils.BeginResize( windowId, "topleft", 800, 500, HopperConfig.OnResizeEnd )
end

function HopperConfig.OnResizeEnd()
	HopperConfig.UpdateDisplayedConfig( currentDisplayedConfig )
end

function HopperConfig.ShowConfig( index )
	if( configuredWindows[index] ~= nil and configuredWindows[index].display ~= nil ) then
		for k,v in ipairs( configuredWindows[index].display )
		do
			v:Show()
		end
	end
end

function HopperConfig.HideConfig( index )
	if( configuredWindows[index] ~= nil and configuredWindows[index].display ~= nil ) then
		for k,v in ipairs( configuredWindows[index].display )
		do
			v:Hide()
		end
	end
end

function HopperConfig.SetConfigParent( index, parent )
	if( configuredWindows[index] ~= nil and configuredWindows[index].display ~= nil ) then
		for k,v in ipairs( configuredWindows[index].display )
		do
			v:Parent( parent )
		end
	end
end

function HopperConfig.ReanchorConfigDisplay( index, initialParent )
	local prevWindow	= initialParent
	local leftAnchor 	= "topleft"
	local rightAnchor 	= "topright"

	if( configuredWindows[index] ~= nil and configuredWindows[index].display ~= nil ) then
		for k,v in ipairs( configuredWindows[index].display )
		do
			v:ClearAnchors()
			v:AddAnchor( prevWindow, leftAnchor, "topleft", 0, 0 )
			v:AddAnchor( prevWindow, rightAnchor, "topright", 0, 0 )
			
			prevWindow = v.name
			leftAnchor = "bottomleft"
			rightAnchor = "bottomright"
		end
	end
end

function HopperConfig.UpdateColorSelection( currentValue, slider, text )
	local changed 		= false
	local sliderColor 	= math.floor( slider:GetValue() )
	local textColor 	= tonumber( text:GetText() ) or sliderColor
	
	if( sliderColor ~= currentValue ) then
		currentValue = sliderColor
		text:SetText( towstring( currentValue ) )
		changed = true
	elseif( textColor ~= currentValue ) then
		currentValue = textColor
		slider:SetValue( currentValue )
		changed = true
	end
		
	return changed, currentValue
end

function HopperConfig.UpdateAlphaSelection( currentValue, slider, text )
	local changed 		= false
	local sliderColor 	= slider:GetValue()
	local textColor 	= tonumber( text:GetText() ) or sliderColor
	
	if( sliderColor ~= currentValue ) then
		currentValue = sliderColor
		text:SetText( wstring.format( L"%.2f", towstring( currentValue ) ) )
		changed = true
	elseif( textColor ~= currentValue ) then
		currentValue = textColor
		slider:SetValue( currentValue )
		changed = true
	end
	
	return changed, currentValue
end