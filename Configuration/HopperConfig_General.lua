--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not HopperConfig then return end

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "Hopper", debug )
local LibGUI 	= LibStub( "LibGUI" )

local initialized		= false
local frameAlpha, slotStatusAlpha
local activeStatusRed, activeStatusGreen, activeStatusBlue
local openGroupRed, openGroupGreen, openGroupBlue
local closedGroupRed, closedGroupGreen, closedGroupBlue

local InitializeWindow, ApplyConfigSettings, RevertConfigSettings
local window	= {
	Name		= T["General"],
	display		= {},
	W			= {},
}
local growthDirections				= HopperConstants.GrowthDirections
local growthDirectionLookup			= HopperConstants.GrowthDirectionsLookup

function InitializeWindow()
	-- If our window already exists, we are all set
	if initialized then return end
	
	local w
	local e	
	
	-- General Window
	w = LibGUI( "window" )
	w:ClearAnchors()
	w:Resize( 400, 305 )
	w:Show( true )
	window.W.General = w
	
	-- Colors Window
	w = LibGUI( "window" )
    w:Resize( 400, 650 )
	w:Show( true )
	window.W.Colors = w
	
	-- This is the order the windows are displayed to the user
	table.insert( window.display, window.W.General )
	table.insert( window.display, window.W.Colors )
	
	--
	-- GENERAL WINDOW
	--
	-- General - Subsection Title
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General, "topleft", "topleft", 15, 10 )
    e:Font( "font_clear_medium_bold" )
    e:SetText( T["General"] )
    window.W.General.Title_Label = e
    
    -- General - Enable Hopper Checkbox
	e = window.W.General( "Checkbox" )
	e:AnchorTo( window.W.General.Title_Label, "bottomleft", "topleft", 10, 5 )
	window.W.General.EnableHopper_Checkbox = e
    
    -- General - Enable Hopper Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.EnableHopper_Checkbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Enable Hopper"] )
    window.W.General.EnableHopper_Label = e
    
    -- General - Show In Scenarios Checkbox
	e = window.W.General( "Checkbox" )
	e:AnchorTo( window.W.General.EnableHopper_Label, "bottomleft", "topleft", 0, 5 )
	window.W.General.ShowInScenario_Checkbox = e
    
    -- General - Show In Scenarios Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.ShowInScenario_Checkbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Show In Scenarios"] )
    window.W.General.ShowInScenario_Label = e
    
    -- General - Show In Warband Checkbox
	e = window.W.General( "Checkbox" )
	e:AnchorTo( window.W.General.ShowInScenario_Checkbox, "bottomleft", "topleft", 0, 5 )
	window.W.General.ShowInWarband_Checkbox = e
    
    -- General - Show In Warband Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.ShowInWarband_Checkbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Show In Warband"] )
    window.W.General.ShowInWarband_Label = e
    
    -- General - Growth Direction Label
    e = window.W.General( "Label" )
    e:Resize( 300 )
    e:AnchorTo( window.W.General.ShowInWarband_Checkbox, "bottomleft", "topleft", 0, 5 )
    e:Font( "font_chat_text" )
    e:Align( "leftcenter" )
    e:SetText( T["Growth Direction:"] )
    window.W.General.GrowthDirection_Label = e
    
    -- General - Growth Direction Combo
    e = window.W.General( "Combobox" )
    e:AnchorTo( window.W.General.GrowthDirection_Label, "bottomleft", "topleft", 0, 0 )
    for _, direction in ipairs( growthDirections ) 
	do 
		e:Add( direction.locale )
	end
    window.W.General.GrowthDirection_Combo = e
    
    -- General - Unit Frame Alpha Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "center" )
    e:AnchorTo( window.W.General, "top", "top", 0, 195 )
    e:Font( "font_chat_text" )
    e:SetText( T["Unit Frame Alpha"] )
    window.W.General.UnitFrameAlpha_Label = e
    
    -- General - Unit Frame Alpha Image
    e = window.W.General( "Image", nil, "HopperDefaultImage" )
    e:Resize( 100, 25 )
    e:AnchorTo( window.W.General.UnitFrameAlpha_Label, "bottom", "top", 0, 5 )
    e:Tint( 255, 255, 255 )
    e:IgnoreInput()
    e:UnregisterDefaultEvents()
    window.W.General.UnitFrameAlpha_Image = e
    
    -- General - Unit Frame Alpha Slider
    e = window.W.General( "Slider" )
    e:AnchorTo( window.W.General.UnitFrameAlpha_Image, "bottom", "top", 0, 5 )
    e:SetRange( 0, 1 )
    window.W.General.UnitFrameAlpha_Slider = e
    
    -- General - Unit Frame Alpha Label
    e = window.W.General( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.General.UnitFrameAlpha_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Alpha"] )
    window.W.General.UnitFrameAlpha_Label = e
    
    -- General - Unit Frame Alpha Textbox
    e = window.W.General( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.General.UnitFrameAlpha_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.General.UnitFrameAlpha_Textbox = e
    
    --
	-- COLORS WINDOW ELEMENTS
	--
	-- Colors - Subsection Title
    e = window.W.Colors( "Label" )
    e:Resize( 550 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.Colors, "topleft", "topleft", 15, 10 )
    e:Font( "font_clear_medium_bold" )
    e:SetText( T["Colors"] )
    window.W.Colors.Title_Label = e

    -- Colors - Active Group Status Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 450 )
    e:Align( "center" )
    e:AnchorTo( window.W.Colors, "top", "top", 0, 30 )
    e:Font( "font_chat_text" )
    e:SetText( T["Active Group Status Color"] )
    window.W.Colors.ActiveGroupStatusColor_Label = e
        
    -- Colors - Active Group Status Red Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.ActiveGroupStatusColor_Label, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.ActiveGroupStatusRed_Slider = e
    
    -- Colors - Active Group Status Red Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.ActiveGroupStatusRed_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Red"] )
    window.W.Colors.ActiveGroupStatusRed_Label = e
    
    -- Colors - Active Group Status Red Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.ActiveGroupStatusRed_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.ActiveGroupStatusRed_Textbox = e
    
    -- Colors - Active Group Status Green Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.ActiveGroupStatusRed_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.ActiveGroupStatusGreen_Slider = e
    
    -- Colors - Active Group Status Green Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.ActiveGroupStatusGreen_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Green"] )
    window.W.Colors.ActiveGroupStatusGreen_Label = e
    
    -- Colors - Active Group Status Green Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.ActiveGroupStatusGreen_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.ActiveGroupStatusGreen_Textbox = e
    
    -- Colors - Active Group Status Blue Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.ActiveGroupStatusGreen_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.ActiveGroupStatusBlue_Slider = e
	
	-- Colors - Active Group Status Blue Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.ActiveGroupStatusBlue_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Blue"] )
    window.W.Colors.ActiveGroupStatusBlue_Label = e
    
    -- Colors - Active Group Status Blue Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.ActiveGroupStatusBlue_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.ActiveGroupStatusBlue_Textbox = e
    
    -- Colors - Open Group Status Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 450 )
    e:Align( "center" )
    e:AnchorTo( window.W.Colors.ActiveGroupStatusBlue_Slider, "bottom", "top", 0, 10 )
    e:Font( "font_chat_text" )
    e:SetText( T["Open Group Status Color"] )
    window.W.Colors.OpenGroupStatusColor_Label = e
        
    -- Colors - Open Group Status Red Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.OpenGroupStatusColor_Label, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.OpenGroupStatusRed_Slider = e
    
    -- Colors - Open Group Status Red Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.OpenGroupStatusRed_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Red"] )
    window.W.Colors.OpenGroupStatusRed_Label = e
    
    -- Colors - Open Group Status Red Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.OpenGroupStatusRed_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.OpenGroupStatusRed_Textbox = e
    
    -- Colors - Open Group Status Green Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.OpenGroupStatusRed_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.OpenGroupStatusGreen_Slider = e
    
    -- Colors - Open Group Status Green Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.OpenGroupStatusGreen_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Green"] )
    window.W.Colors.OpenGroupStatusGreen_Label = e
    
    -- Colors - Open Group Status Green Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.OpenGroupStatusGreen_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.OpenGroupStatusGreen_Textbox = e
    
    -- Colors - Open Group Status Blue Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.OpenGroupStatusGreen_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.OpenGroupStatusBlue_Slider = e
	
	-- Colors - Open Group Status Blue Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.OpenGroupStatusBlue_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Blue"] )
    window.W.Colors.OpenGroupStatusBlue_Label = e
    
    -- Colors - Open Group Status Blue Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.OpenGroupStatusBlue_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.OpenGroupStatusBlue_Textbox = e
    
    -- Colors - Closed Group Status Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 450 )
    e:Align( "center" )
    e:AnchorTo( window.W.Colors.OpenGroupStatusBlue_Slider, "bottom", "top", 0, 10 )
    e:Font( "font_chat_text" )
    e:SetText( T["Closed Group Status Color"] )
    window.W.Colors.ClosedGroupStatusColor_Label = e
        
    -- Colors - Closed Group Status Red Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.ClosedGroupStatusColor_Label, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.ClosedGroupStatusRed_Slider = e
    
    -- Colors - Closed Group Status Red Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.ClosedGroupStatusRed_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Red"] )
    window.W.Colors.ClosedGroupStatusRed_Label = e
    
    -- Colors - Closed Group Status Red Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.ClosedGroupStatusRed_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.ClosedGroupStatusRed_Textbox = e
    
    -- Colors - Closed Group Status Green Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.ClosedGroupStatusRed_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.ClosedGroupStatusGreen_Slider = e
    
    -- Colors - Closed Group Status Green Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.ClosedGroupStatusGreen_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Green"] )
    window.W.Colors.ClosedGroupStatusGreen_Label = e
    
    -- Colors - Closed Group Status Green Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.ClosedGroupStatusGreen_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.ClosedGroupStatusGreen_Textbox = e
    
    -- Colors - Closed Group Status Blue Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.ClosedGroupStatusGreen_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.ClosedGroupStatusBlue_Slider = e
	
	-- Colors - Closed Group Status Blue Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.ClosedGroupStatusBlue_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Blue"] )
    window.W.Colors.ClosedGroupStatusBlue_Label = e
    
    -- Colors - Closed Group Status Blue Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.ClosedGroupStatusBlue_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.ClosedGroupStatusBlue_Textbox = e
    
    -- Colors - Unit Frame Alpha Label
    e = window.W.Colors( "Label" )
    e:Resize( 450 )
    e:Align( "center" )
    e:AnchorTo( window.W.Colors, "top", "top", 0, 485 )
    e:Font( "font_chat_text" )
    e:SetText( T["Slot Status Alpha"] )
    window.W.Colors.SlotStatusAlpha_Label = e
    
    -- Colors - Unit Frame Alpha Image
    e = window.W.Colors( "Image", nil, "HopperDefaultImage" )
    e:Resize( 100, 25 )
    e:AnchorTo( window.W.Colors.SlotStatusAlpha_Label, "bottom", "top", 0, 5 )
    e:Tint( 255, 255, 255 )
    e:UnregisterDefaultEvents()
    e:IgnoreInput()
    window.W.Colors.SlotStatusAlpha_Image = e
    
    -- Colors - Unit Frame Alpha Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.SlotStatusAlpha_Image, "bottom", "top", 0, 5 )
    e:SetRange( 0, 1 )
    window.W.Colors.SlotStatusAlpha_Slider = e
    
    -- Colors - Unit Frame Alpha Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.SlotStatusAlpha_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Alpha"] )
    window.W.Colors.SlotStatusAlpha_Label = e
    
    -- Colors - Unit Frame Alpha Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.SlotStatusAlpha_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.SlotStatusAlpha_Textbox = e
    
   initialized = true
end

function ApplyConfigSettings()
	--
	-- General
	--
	Hopper.Set( "general-enable-addon", window.W.General.EnableHopper_Checkbox:GetValue() )
	Hopper.Set( "general-scenario-enable", window.W.General.ShowInScenario_Checkbox:GetValue() )
	Hopper.Set( "general-warband-enable", window.W.General.ShowInWarband_Checkbox:GetValue() )
	Hopper.Set( "hopper-fill-direction", growthDirections[window.W.General.GrowthDirection_Combo:SelectedIndex()].config )
	Hopper.Set( "general-frame-alpha", frameAlpha )
	
	--
	-- Color
	--
	Hopper.Set( "activegroup-status-color", { activeStatusRed, activeStatusGreen, activeStatusBlue } )
	Hopper.Set( "slot-open-status-color", { openGroupRed, openGroupGreen, openGroupBlue } )
	Hopper.Set( "slot-closed-status-color", { closedGroupRed, closedGroupGreen, closedGroupBlue } )
	Hopper.Set( "slot-status-alpha", slotStatusAlpha )
end

function RevertConfigSettings()
	window.W.General.EnableHopper_Checkbox:SetValue( Hopper.Get( "general-enable-addon" ) )
	window.W.General.ShowInScenario_Checkbox:SetValue( Hopper.Get( "general-scenario-enable" ) )
	window.W.General.ShowInWarband_Checkbox:SetValue( Hopper.Get( "general-warband-enable" ) )
	window.W.General.GrowthDirection_Combo:SelectIndex( growthDirectionLookup[Hopper.Get( "hopper-fill-direction" )] )
	
	frameAlpha = Hopper.Get( "general-frame-alpha" )
	window.W.General.UnitFrameAlpha_Slider:SetValue( frameAlpha )
	window.W.General.UnitFrameAlpha_Textbox:SetText( wstring.format( L"%.2f", towstring( frameAlpha ) ) )
	window.W.General.UnitFrameAlpha_Image:Alpha( frameAlpha )
	
	activeStatusRed, activeStatusGreen, activeStatusBlue = unpack( Hopper.Get( "activegroup-status-color" ) )
	window.W.Colors.ActiveGroupStatusColor_Label:Color( activeStatusRed, activeStatusGreen, activeStatusBlue )
	window.W.Colors.ActiveGroupStatusRed_Slider:SetValue( activeStatusRed )
	window.W.Colors.ActiveGroupStatusRed_Textbox:SetText( towstring( activeStatusRed ) )
	window.W.Colors.ActiveGroupStatusGreen_Slider:SetValue( activeStatusGreen )
    window.W.Colors.ActiveGroupStatusGreen_Textbox:SetText( towstring( activeStatusGreen ) )
    window.W.Colors.ActiveGroupStatusBlue_Slider:SetValue( activeStatusBlue )
    window.W.Colors.ActiveGroupStatusBlue_Textbox:SetText( towstring( activeStatusBlue ) )
    
    openGroupRed, openGroupGreen, openGroupBlue = unpack( Hopper.Get( "slot-open-status-color" ) )
	window.W.Colors.OpenGroupStatusColor_Label:Color( openGroupRed, openGroupGreen, openGroupBlue )
	window.W.Colors.OpenGroupStatusRed_Slider:SetValue( openGroupRed )
	window.W.Colors.OpenGroupStatusRed_Textbox:SetText( towstring( openGroupRed ) )
	window.W.Colors.OpenGroupStatusGreen_Slider:SetValue( openGroupGreen )
    window.W.Colors.OpenGroupStatusGreen_Textbox:SetText( towstring( openGroupGreen ) )
    window.W.Colors.OpenGroupStatusBlue_Slider:SetValue( openGroupBlue )
    window.W.Colors.OpenGroupStatusBlue_Textbox:SetText( towstring( openGroupBlue ) )
    
    closedGroupRed, closedGroupGreen, closedGroupBlue = unpack( Hopper.Get( "slot-closed-status-color" ) )
	window.W.Colors.ClosedGroupStatusColor_Label:Color( closedGroupRed, closedGroupGreen, closedGroupBlue )
	window.W.Colors.ClosedGroupStatusRed_Slider:SetValue( closedGroupRed )
	window.W.Colors.ClosedGroupStatusRed_Textbox:SetText( towstring( closedGroupRed ) )
	window.W.Colors.ClosedGroupStatusGreen_Slider:SetValue( closedGroupGreen )
    window.W.Colors.ClosedGroupStatusGreen_Textbox:SetText( towstring( closedGroupGreen ) )
    window.W.Colors.ClosedGroupStatusBlue_Slider:SetValue( closedGroupBlue )
    window.W.Colors.ClosedGroupStatusBlue_Textbox:SetText( towstring( closedGroupBlue ) )
    
    slotStatusAlpha	= Hopper.Get( "slot-status-alpha" )
	window.W.Colors.SlotStatusAlpha_Slider:SetValue( slotStatusAlpha )
	window.W.Colors.SlotStatusAlpha_Textbox:SetText( wstring.format( L"%.2f", towstring( slotStatusAlpha ) ) )
	window.W.Colors.SlotStatusAlpha_Image:Alpha( slotStatusAlpha )
end

function HopperConfig_General_OnUpdate()
	if( HopperConfig.IsShowing() and ( HopperConfig.GetActiveConfigWindowIndex() ==  window.Index ) and initialized ) then
		local updated = false
		local ret = false
		
		--
		-- General
		--
		ret, frameAlpha = HopperConfig.UpdateAlphaSelection( frameAlpha, window.W.General.UnitFrameAlpha_Slider, window.W.General.UnitFrameAlpha_Textbox )
		updated = updated or ret
		
		if( updated ) then
			window.W.General.UnitFrameAlpha_Image:Alpha( frameAlpha )
			updated = false
		end
		
		--
		-- Colors
		--
		ret, activeStatusRed = HopperConfig.UpdateColorSelection( activeStatusRed, window.W.Colors.ActiveGroupStatusRed_Slider, window.W.Colors.ActiveGroupStatusRed_Textbox )
		updated = updated or ret
		
		ret, activeStatusGreen = HopperConfig.UpdateColorSelection( activeStatusGreen, window.W.Colors.ActiveGroupStatusGreen_Slider, window.W.Colors.ActiveGroupStatusGreen_Textbox )
		updated = updated or ret
		
		ret, activeStatusBlue = HopperConfig.UpdateColorSelection( activeStatusBlue, window.W.Colors.ActiveGroupStatusBlue_Slider, window.W.Colors.ActiveGroupStatusBlue_Textbox )
		updated = updated or ret
		
		-- Update our preview color
		if( updated ) then
			window.W.Colors.ActiveGroupStatusColor_Label:Color( activeStatusRed, activeStatusGreen, activeStatusBlue )
			updated = false
		end
		
		ret, openGroupRed = HopperConfig.UpdateColorSelection( openGroupRed, window.W.Colors.OpenGroupStatusRed_Slider, window.W.Colors.OpenGroupStatusRed_Textbox )
		updated = updated or ret
		
		ret, openGroupGreen = HopperConfig.UpdateColorSelection( openGroupGreen, window.W.Colors.OpenGroupStatusGreen_Slider, window.W.Colors.OpenGroupStatusGreen_Textbox )
		updated = updated or ret
		
		ret, openGroupBlue = HopperConfig.UpdateColorSelection( openGroupBlue, window.W.Colors.OpenGroupStatusBlue_Slider, window.W.Colors.OpenGroupStatusBlue_Textbox )
		updated = updated or ret
		
		-- Update our preview color
		if( updated ) then
			window.W.Colors.OpenGroupStatusColor_Label:Color( openGroupRed, openGroupGreen, openGroupBlue )
			updated = false
		end
		
		ret, closedGroupRed = HopperConfig.UpdateColorSelection( closedGroupRed, window.W.Colors.ClosedGroupStatusRed_Slider, window.W.Colors.ClosedGroupStatusRed_Textbox )
		updated = updated or ret
		
		ret, closedGroupGreen = HopperConfig.UpdateColorSelection( closedGroupGreen, window.W.Colors.ClosedGroupStatusGreen_Slider, window.W.Colors.ClosedGroupStatusGreen_Textbox )
		updated = updated or ret
		
		ret, closedGroupBlue = HopperConfig.UpdateColorSelection( closedGroupBlue, window.W.Colors.ClosedGroupStatusBlue_Slider, window.W.Colors.ClosedGroupStatusBlue_Textbox )
		updated = updated or ret
		
		-- Update our preview color
		if( updated ) then
			window.W.Colors.ClosedGroupStatusColor_Label:Color( closedGroupRed, closedGroupGreen, closedGroupBlue )
			updated = false
		end
		
		ret, slotStatusAlpha = HopperConfig.UpdateAlphaSelection( slotStatusAlpha, window.W.Colors.SlotStatusAlpha_Slider, window.W.Colors.SlotStatusAlpha_Textbox )
		updated = updated or ret
		
		if( updated ) then
			window.W.Colors.SlotStatusAlpha_Image:Alpha( slotStatusAlpha )
			updated = false
		end
	end
end

window.Initialize	= InitializeWindow
window.Apply		= ApplyConfigSettings
window.Revert		= RevertConfigSettings
window.Index		= HopperConfig.RegisterWindow( window )